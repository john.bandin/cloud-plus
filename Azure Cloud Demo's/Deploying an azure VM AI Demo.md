# Create a Virtual Machine in Azure
#### [Made by John Bandin with Scribe](https://scribehow.com/shared/Create_a_Virtual_Machine_in_Azure__-LPajonITzS5kMp4JP_6Fw)


1\. Navigate to https://portal.azure.com/#home


2\. Click "Create a Resource"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/34a0aac3-c9f1-4b5c-89ec-67cf7a505b3d/user_cropped_screenshot.jpeg?tl_px=6,0&br_px=865,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,111)


3\. Click "Virtual machine"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/5b7854d5-e99f-4f8d-8f0a-7bd7b6981aac/user_cropped_screenshot.jpeg?width=860&height=480&force_format=png&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=308,212)


4\. Choose a "subscription" 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/fda931b4-e946-46eb-9c45-1e61d7982800/ascreenshot.jpeg?tl_px=0,194&br_px=859,675&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=385,212)


5\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/da023f4a-af95-46f0-913b-2afad1e56251/ascreenshot.jpeg?tl_px=0,253&br_px=859,734&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=376,212)


6\. Click "(New) Resource group"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/69cdcec3-b72c-4ef6-b14e-7d6c0b603d7e/ascreenshot.jpeg?tl_px=0,230&br_px=859,711&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=356,212)


7\. Now choose your "Resource Group"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/4858e7fc-4d6c-403c-ae93-36ff82ba136f/ascreenshot.jpeg?tl_px=0,332&br_px=859,813&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=366,212)


8\. Click the "Virtual machine name" field.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/dab65c39-aa1a-4d51-bafd-945dec70e410/ascreenshot.jpeg?tl_px=0,332&br_px=859,813&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=366,212)


9\. Type your virtual machine name.


10\. Choose what region you want to deploy your virtual machine in.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/afbdc69a-7638-4570-8bb0-cb71eecc4a1e/ascreenshot.jpeg?tl_px=199,367&br_px=1059,848&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,212)


11\. Now Choose what "Operating System" you want to deploy and manage.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/efb7aadd-7214-4cfb-994f-e51f26952ef2/ascreenshot.jpeg?tl_px=62,75&br_px=922,556&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,212)


12\. Now choose your "compute" resources. We will choose the "free tier services". 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/2f8ca24d-c1e0-40dc-99ae-fb276732f489/ascreenshot.jpeg?tl_px=62,535&br_px=921,1016&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,212)


13\. Click the "Key pair name" field and set a name for the "Public/Private" key-pairs that will be created for this VM&gt;

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/bdd22933-c7a3-43a2-8440-7a9700ce95b1/ascreenshot.jpeg?tl_px=0,470&br_px=859,951&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=337,212)


14\. Click "Next : Disks >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/884353b7-061f-4ba0-8fd6-893d16dce507/ascreenshot.jpeg?tl_px=0,677&br_px=859,1158&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=369,413)


15\. Now let's choose what type of "disk" we want. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/34e3c7f5-8337-41cd-a818-09ef267b92c5/ascreenshot.jpeg?tl_px=0,382&br_px=859,863&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=399,212)


16\. Click "Next : Networking >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/a3b23b95-eb9c-442e-b930-c01e9fc042f4/ascreenshot.jpeg?tl_px=0,677&br_px=859,1158&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=381,411)


17\. Now we will choose our "networking" settings. We may need to create a new private IP subnet if this is our first time deploying a VM on our Azure subscription.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/a6cb1a9e-d630-4c9a-8e44-cacf0deeacd5/ascreenshot.jpeg?tl_px=0,108&br_px=859,589&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=399,212)


18\. Click "(new) for-class-02-vnet"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/7095cf4a-5dce-4fda-82f9-e491a58d66a3/ascreenshot.jpeg?tl_px=0,176&br_px=859,657&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=388,212)


19\. Click "(new) default (10.0.0.0/24)"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/1240755b-679b-43a3-a491-0cfaf97549fb/ascreenshot.jpeg?tl_px=0,164&br_px=859,645&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=367,212)


20\. Click "(new) default (10.0.0.0/24)"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/202c5ff8-287a-4017-a926-df1803b8083a/ascreenshot.jpeg?tl_px=0,227&br_px=859,708&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=366,212)


21\. If we want too, we may choose to set a static IP on our VM.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/5e33efbb-e4da-4258-8724-b0c29407b15d/ascreenshot.jpeg?tl_px=0,210&br_px=859,691&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=370,212)


22\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/bc3119d8-db9a-43e6-b70e-d2357d1511c6/ascreenshot.jpeg?tl_px=0,220&br_px=859,701&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=378,212)


23\. Click "Next : Management >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/3ab2877b-8159-46bf-b286-dd970643ef74/ascreenshot.jpeg?tl_px=0,677&br_px=859,1158&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=386,414)


24\. Here we can choose what kind of IAM we want for this VM.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/6589bc37-fdb6-4d81-9cf1-16b01ab4dbf0/ascreenshot.jpeg?tl_px=0,188&br_px=859,669&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=258,212)


25\. We will also enable "auto-shutdown" to save money on our subscription.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/a23cf5db-66e0-42b8-8d09-177de5d71b61/ascreenshot.jpeg?tl_px=0,454&br_px=859,935&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=266,212)


26\. Click "Next : Monitoring >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/e75bfaaf-6bfe-4e34-9bd4-85afc494ac3c/ascreenshot.jpeg?tl_px=0,677&br_px=859,1158&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,416)


27\. Click "Next : Advanced >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/1dc4f45c-51f2-4b3b-abdf-bc749516d6d1/ascreenshot.jpeg?tl_px=0,677&br_px=859,1158&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=399,417)


28\. Click "Next : Tags >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/3e27e882-4fb5-4cc4-a7ad-63fd9164f4ab/ascreenshot.jpeg?tl_px=0,677&br_px=859,1158&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=390,415)


29\. Click "Review + create"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/b59aa487-f9a1-4e50-971e-5c0d8c67a39b/ascreenshot.jpeg?tl_px=0,677&br_px=859,1158&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=103,419)


30\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/fee30385-98ff-4844-8a52-a0f426ff329f/ascreenshot.jpeg?tl_px=0,677&br_px=859,1158&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=63,423)


31\. Click "Download private key and create resource"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2023-09-17/5c719f0a-9643-4366-b5c9-ce3f3b1a744f/ascreenshot.jpeg?tl_px=532,481&br_px=1392,962&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,212)


32\. And now we will wait for our VM to be fully deployed!
#### [Made with Scribe](https://scribehow.com/shared/Create_a_Virtual_Machine_in_Azure__-LPajonITzS5kMp4JP_6Fw)


