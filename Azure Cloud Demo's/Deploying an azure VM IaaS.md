# Lab Title: Deploying an Azure Virtual Machine

## Objective
In this lab, you will learn how to deploy a virtual machine in Microsoft Azure.

## Prerequisites
1. An active Microsoft Azure subscription.
2. Basic familiarity with Azure Portal.

## Lab Steps

### Step 1: Sign In to Azure Portal
- Open a web browser and go to the [Azure portal](https://portal.azure.com/).
- Sign in with your Azure account credentials.

### Step 2: Create a Virtual Machine
1. In the Azure portal, click on the "+ Create a resource" button.
2. In the search box, type "Windows Server" (or "Linux" if you prefer a Linux VM).
3. Select the desired operating system image (e.g., "Windows Server 2022 Datacenter" or your preferred Linux distribution).
4. Click the "Create" button to begin configuring the virtual machine.

### Step 3: Configure Basics
- Fill out the basic settings for your VM:
   - **Project details:** Choose your subscription, resource group, and region.
   - **Instance details:** Enter a unique name for your VM.
   - **Administrator account:** Set a username and password for accessing the VM.
   - **Inbound port rules:** Configure the port(s) you want to allow access to (e.g., RDP for Windows or SSH for Linux).
   - Review the summary and click "Next: Disks" to proceed.

### Step 4: Configure Disks
- Customize disk settings:
   - **OS disk type:** Choose between Standard HDD, Standard SSD, or Premium SSD.
   - **Enable encryption if needed.**
   - Click "Next: Networking" to continue.

### Step 5: Configure Networking
- Configure networking settings:
   - **Virtual network:** Select an existing virtual network or create a new one.
   - **Subnet:** Choose a subnet within the virtual network.
   - **Public IP:** Assign a public IP address to your VM (if required).
   - **Network security group:** Choose an existing NSG or create a new one.
   - Review the settings and click "Next: Management" to proceed.

### Step 6: Configure Management
- Configure management settings:
   - **Boot diagnostics:** Enable or disable boot diagnostics.
   - **Auto-shutdown:** Set a time for automatic VM shutdown (if needed).
   - Review the settings and click "Next: Advanced" to continue.

### Step 7: Configure Advanced
- Configure advanced settings (optional):
   - **Extensions:** Add any extensions you need (e.g., custom script extensions).
   - **Tags:** Apply tags for resource organization.
   - Click "Review + create" to review your configuration.

### Step 8: Review and Create
- Review all the settings for your VM.
- If everything looks correct, click the "Create" button to start deploying the VM.

### Step 9: Deployment
- Wait for the deployment to complete. You can monitor the progress in the Azure portal.

### Step 10: Access the VM
- Once the deployment is complete, you can access the VM using RDP (Windows) or SSH (Linux) with the provided credentials.

## Conclusion
You have successfully deployed a virtual machine in Microsoft Azure. You can now use this VM for various purposes, such as hosting a web application, running services, or conducting further Azure-related experiments.
